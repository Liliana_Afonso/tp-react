import React from 'react';
import Image from './Image';
import Title from './Title';
import PersonalData from './PersonalData';
import PropTypes from 'prop-types';

const App = ({data}) => {
    return (
        <div className="content">
            <Image image={data.picture.large}/>
            <Title firstname={data.name.first} lastname={data.name.last} />
            <PersonalData
                userName={data.login.username}
                age={data.dob.age}
                mail={data.email}
                phone={data.phone}
            />
        </div>
    );
};

App.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object),
};

export default App;
