import React from 'react';
import PropTypes from 'prop-types';


const Image = ({image}) => {
    return (
        <div className="profileImage">
            <img src={image} alt=""></img>
         </div>
    );
};


Image.propTypes = {
    image: PropTypes.string,
};

export default Image;
