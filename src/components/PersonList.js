import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import App from './App';
// import PropTypes from 'prop-types';

const ReactRoot = document.getElementById('root');
axios.get('https://randomuser.me/api/')
.then(response => {
    const {data} = response;
    console.log(data.results[0]);
    ReactDOM.render(<App data={data.results[0]} />, ReactRoot);
})
.catch(error => console.error(error));


// ReactRoot.propTypes = {
//     data: PropTypes.arrayOf(PropTypes.object),
// };

export default ReactRoot;
