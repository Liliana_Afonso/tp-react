import React from 'react';
import PropTypes from 'prop-types';

const PersonalData = ({userName, age, mail, phone}) => {
    const handleClick = (e) => {
        console.log("Vous avez cliqué sur la liste");
        document.getElementsByClassName("test")[0].style.textDecoration="line-through";
        // e.style.textDecoration="line-through";
        // e.preventDefault();
    };
    const handleClick1 = () => {
        console.log("Vous avez cliqué sur la liste1");
        document.getElementsByClassName("test2")[0].style.textDecoration="line-through";
    };
    const handleClick2 = () => {
        console.log("Vous avez cliqué sur la liste2");
        document.getElementsByClassName("test3")[0].style.textDecoration="line-through";
    };
    const handleClick3 = () => {
        console.log("Vous avez cliqué sur la liste3");
        document.getElementsByClassName("test4")[0].style.textDecoration="line-through";
    };
    return (
        <ul>
            <li className="test" onClick={handleClick}>Nom d'utilisateur: {userName}</li>
            <li className="test2" onClick={handleClick1}>Age: {age}</li>
            <li className="test3" onClick={handleClick2}>Email: {mail}</li>
            <li className="test4" onClick={handleClick3} >Tél: {phone}</li>
        </ul>
    );
};


PersonalData.propTypes = {
    userName: PropTypes.string,
    age: PropTypes.number,
    mail: PropTypes.string,
    phone: PropTypes.string,
};

export default PersonalData;
