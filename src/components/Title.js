import React from 'react';
import PropTypes from 'prop-types';


const Title = ({firstname, lastname}) => {
    return (
        <h1>{firstname} {lastname} </h1>
    );
};


Title.propTypes = {
    firstname: PropTypes.string,
    lastname: PropTypes.string,
};

export default Title;
