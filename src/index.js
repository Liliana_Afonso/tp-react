import React from "react";
import ReactDOM from "react-dom";
import ReactRoot from './components/PersonList';

ReactDOM.render(<ReactRoot />, document.getElementById('root'));

module.hot.accept();
